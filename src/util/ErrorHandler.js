
export class ErrorHandler {

	static handleError(e, errorCallback) {

		if (errorCallback) {
			const response = e.response;
			if (response == null) {
				console.error(e);
				errorCallback("Unexpected error occurred. Please try again later.", "Error", 'error');
			}
			else {
				const status = e.response.status;
				if (status >= 400 && status < 500) {
					console.error(e.response);
					errorCallback(e.response.data, "Warning", 'warn');
				} else {
					console.error(e);
					errorCallback("Unexpected error occurred. Please try again later.", "Error", 'error');
				}
			}
		}
	}

}