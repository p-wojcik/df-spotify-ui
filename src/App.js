import React from 'react';
import './App.css';
import "primeflex/primeflex.css"
import 'primeicons/primeicons.css';
import 'primereact/resources/primereact.min.css';
import 'primereact/resources/themes/luna-green/theme.css';
import HomeComponent from './home/HomeComponent';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Security, ImplicitCallback } from '@okta/okta-react';

const CONFIG = require('./config.json');

function onAuthRequired({history}) {
  	history.push('/login');
}

export default function App() {
	return (
		<Router>
			<Security
					issuer={CONFIG.auth.issuer} clientId={CONFIG.auth.clientId} redirectUri={CONFIG.auth.redirectUri}
					onAuthRequired={onAuthRequired} pkce={true}>
				<HomeComponent />
				<Route path='/implicit/callback' component={ImplicitCallback} />
			</Security>
		</Router>
	);
}
