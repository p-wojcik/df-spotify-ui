import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import autoBind from 'react-autobind';
const _ = require('lodash');

export class ArrivalsContainer extends Component {

    constructor() {
        super();
        this.state = {
        	filter: "",
            items: [
            	{
            		"id" : "abc123",
            		"title" : "Mezzanine",
            		"type" : "TRACK",
            		"details" : {
            			"artist" : "Massive Attack",
            			"album": "Mezzanine",
            			"year": 1997
            		},
            		"sharingDate" : "xxx",
                    "author": {
                        "id" : "234567",
                        "displayName" : "bbhabhj sdfsd"
                    },
				  	"message" : "asdas",
            		"spotifyURI" : "some_uri"
            	},
            	{
					"id" : "abc12s3",
					"title" : "Downward Spiral",
					"type" : "ALBUM",
					"details" : {
						"artist" : "Nine inch nails",
						"year": 1994
					},
					"sharingDate" : "xxx",
                    "author": {
                        "id" : "23xxx4567",
                        "displayName" : "bbhabhj sdfsd",
                    },
				  	"message" : "asdas",
					"spotifyURI" : "some_uri"
				}
            ]
        };
        autoBind(this);
    }

    playbackTemplate(rowData, column) {
        return (
        	<Button tooltip="Play" icon="pi pi-caret-right" className="p-button-warning" style={{'margin' : '0 5px'}}/>
        );
    }

    titleTemplate(rowData, column) {
    	const type = rowData.type;
    	const details = rowData.details;
    	if (type === 'TRACK') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.artist}</i> from album <i>{details.album}, {details.year}</i>
				</>
			);
    	} else if (type === 'ALBUM') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.artist}, {details.year}</i>
				</>
			);
    	} else if (type === 'ARTIST') {
			return <b>{rowData.title}</b>;
		} else if (type === 'PLAYLIST') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.owner}</i>
				</>
			);
		} else {
			return "";
		}
    }

    typeTemplate(rowData, color) {
    	return _.startCase(_.toLower(rowData.type));
    }

    publishPlaylist(playlistId, isPublic) {
    	const freshCopy = _.cloneDeep(this.state.items);
    	const index = _.findIndex(freshCopy.items, {'id' : playlistId});
      	freshCopy.items[index].public = isPublic;
      	this.setState({items: freshCopy});
    }

	actionsTemplate(rowData, column) {
		return (
			<>
				<Button tooltip="Read note" icon="pi pi-inbox" className="p-button-info" />
				<Button tooltip="Copy Spotify URI" icon="pi pi-copy" className="p-button-warning" style={{'margin' : '0 5px'}}/>
				<Button tooltip="Remove content" icon="pi pi-trash" className="p-button-danger" />
			</>
		);
	}

    prepareHeader()
    {
    	return (
    		<div className="p-clearfix" style={{'lineHeight':'1.87em'}}>
    			<span className="pi pi-inbox" style={{'color': '#007ad9', 'float' : 'left', 'fontSize': '2em'}}></span>
    			<span style={{ 'float' : 'left', 'fontSize': '1.5em'}}>Arrivals</span>
    			<div style={{ 'float' : 'right'}}>
					<InputText type="search" onInput={(e) => this.setState({filter: e.target.value})} placeholder="Filter..." size="30"/>
    				<Button label="Refresh" icon="pi pi-refresh" style={{'marginLeft' : '5px'}} />
    			</div>
			</div>
    	);
    }

    render() {
        const header = this.prepareHeader();
        return (
            <div className="contentTable">
                <div className="content-section implementation">
                    <DataTable globalFilter={this.state.filter} value={this.state.items} header={header}
                    	emptyMessage="No items found" scrollable={true} scrollHeight="30em">
                        <Column header="Playback" body={this.playbackTemplate} style={{width: '6em'}} />
                        <Column header="Title" field="title" body={this.titleTemplate} />
                        <Column body={this.typeTemplate} field="type" header="Type" style={{width: '12em'}} />
                        <Column field="author.displayName" header="Author" style={{width: '12em'}}/>
                        <Column header="Actions" body={this.actionsTemplate} style={{width: '10em'}} />
                    </DataTable>
                </div>
            </div>
        );
    }
}