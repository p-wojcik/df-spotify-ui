import React, {Component} from 'react';
import {Growl} from 'primereact/growl';
import { LoginComponent } from '../components/LoginComponent.js';
import { AddNewContentComponent } from '../components/AddNewContentComponent.js';
import { BrowseSpotifyHeaderComponent } from '../components/BrowseSpotifyHeaderComponent.js';
import { BoxContainer } from './BoxContainer.js';
import { DevicesContainer } from './DevicesContainer.js';
import { ArrivalsContainer } from './ArrivalsContainer.js';
import { PlaylistsContainer } from './PlaylistsContainer.js';
import autoBind from 'react-autobind';
const {AuthenticationAPI} = require('../api/AuthenticationAPI.js');

export class RootContainer extends Component {

	constructor() {
		super();
		this.state = {
			activeDeviceId : "",
			currentlyPlaying: "",
			authenticated: false
		};
		this.api = new AuthenticationAPI();
		autoBind(this);
	}

   	componentDidMount() {
		this.introduce();
	}

	introduce() {
		this.api.introduce(() => this.setState({authenticated: true}), () => this.setState({authenticated: false}));
	}

    showCustomGrowl(msg, title, type) {
		this.growl.show({severity: type, summary: title, detail: msg});
    }

	setActiveDeviceId(v) {
		this.setState({activeDeviceId : v});
	}

	render() {
		return this.state.authenticated ? this.prepareSecuredContent() : this.prepareLoginComponent();
	}

	prepareLoginComponent() {
		return <LoginComponent />;
	}

	prepareSecuredContent() {
		return (
			<>
				<Growl ref={(el) => this.growl = el} />
				<div className="p-grid">
					<div className="p-col">
						<BrowseSpotifyHeaderComponent />
					</div>
					<div className="p-col">
						<AddNewContentComponent />
					</div>
				</div>
				<br/>
				<ArrivalsContainer />
				<br/>
				<BoxContainer activeDeviceId={this.state.activeDeviceId}
							  changePlaybackState={(e) => this.setState({currentlyPlaying : e})}
				/>
				<br/>
				<DevicesContainer activeDeviceId={this.state.activeDeviceId}
								  setActiveDeviceCallback={(v) => this.setState({activeDeviceId : v})}
								  currentlyPlaying={this.state.currentlyPlaying}
				/>
				<br/>
				<PlaylistsContainer />
			</>
		);
	}

}