import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import {Growl} from 'primereact/growl';
import autoBind from 'react-autobind';
const {PlaybackAPI} = require('../api/PlaybackAPI.js');
const {PlaylistsAPI} = require('../api/PlaylistsAPI.js');
const _ = require('lodash');
const CONFIG = require('../../config.json');

export class BoxContainer extends Component {

    constructor()
    {
        super();
        this.state = {
        	filter: "",
			currentlyPlaying : "",
            items: [
            	{
					"id": "5384d43a-e758-4efe-b06f-1ec25bc1d9e6",
					"title": "Mezzanine",
					"type": "TRACK",
					"details": {
						"artist": "Massive Attack",
						"album": "Mezzanine",
						"year": 1998,
						"appearsOn" : [
							{
								"id" : "1BAsINSfFQjQAAvtiXAmb1",
								"name" : "t2"
							}
						]
					},
					"shares": {
						"users": 0,
						"groups": 0
					},
					"boxId" : "233f023a-8a46-440a-89e8-ba16bbdbe866",
					"shareableType": "SPOTIFY",
					"creationTimestamp" : 1571927871,
					"spotifyURI": "spotify:track:4IfHYniNBoAATWV6iLEXRs"
                },
            	{
					"id" : "abc12s3",
					"title" : "Downward Spiral",
					"type" : "ALBUM",
					"details" : {
						"artist" : "Nine inch nails",
						"year": 1994
					},
					"shares" : {
						"users" : 13,
						"groups" : 0
					},
					"spotifyURI" : "some_uri"
				},
				{
					"id" : "abc12s3",
					"title" : "Pixies",
					"type" : "ARTIST",
					"shares" : {
						"users" : 0,
						"groups" : 3
					},
					"spotifyURI" : "some_uri"
				},
				{
				  "id" : "5384d43a-e758-4efe-cccc-1ec25bc1d9ec",
				  "title" : "501",
				  "type" : "PLAYLIST",
				  "details" : {
				  	"spotifyId" : "5YQBmH8RizuD3YPBhs2gj6",
					"owner" : "Paulina Wójcik",
					"ownerId": "553f023a-8a46-440a-89e8-ba16bbdbe866",
				  	"followed" : true
				  },
				  "shares" : {
					"users" : 0,
					"groups" : 0
				  },
				  "boxId" : "233f023a-8a46-440a-89e8-ba16bbdbe866",
				  "shareableType": "SPOTIFY",
				  "creationTimestamp" : 1571927871,
				  "spotifyURI" : "spotify:playlist:5YQBmH8RizuD3YPBhs2gj6"
                },
				{
				  "id" : "5384d43a-e758-4efe-b06f-1ec25bc1d9ec",
				  "title" : "xxxxxyyyyy",
				  "type" : "PLAYLIST",
				  "details" : {
					"owner" : "Paulina Wójcik",
					"ownerId": "1171686443",
				  	"followed" : true
				  },
				  "shares" : {
					"users" : 0,
					"groups" : 0
				  },
				  "boxId" : "233f023a-8a46-440a-89e8-ba16bbdbe866",
				  "shareableType": "SPOTIFY",
				  "creationTimestamp" : 1571927871,
				  "spotifyURI" : "spotify:playlist:5YQBmH8RizuD3YPBhs2gj6"
                }
            ]
        };
        this.playbackAPI = new PlaybackAPI();
        this.playlistsAPI = new PlaylistsAPI();
        autoBind(this);
    }

    sendStartPlaybackRequest(item, activeDeviceId, callback)
    {
    	this.playbackAPI.startPlayback(item, activeDeviceId, () => {
			callback(true);
			this.setState({currentlyPlaying : item.spotifyURI})
		}, this.showCustomGrowl);
    }

	sendStopPlaybackRequest(callback)
	{
		this.playbackAPI.stopPlayback(() => {
			callback(false);
			this.setState({currentlyPlaying : ""});
		}, this.showCustomGrowl);
	}

    followPlaylist(playlistId) {
    	this.playlistsAPI.followPlaylist(playlistId,
    	() => this.showCustomGrowl('Playlist is now being followed.','Success','success'), this.showCustomGrowl);
    }

    unfollowPlaylist(playlistId) {
    	this.playlistsAPI.unfollowPlaylist(playlistId,
    	() => this.showCustomGrowl('Playlist has been un-followed.','Success','success'), this.showCustomGrowl);
    }

	showCustomGrowl(msg, title, type) {
		this.growl.show({severity: type, summary: title, detail: msg});
	}

	showCopyConfirmation()
	{
		this.growl.show({severity: 'success', summary: 'Success', detail: 'Copied to clipboard!'});
	}

    playbackTemplate(rowData, column, activeDeviceId, changePlaybackState)
    {
    	const isPlayed = rowData.spotifyURI === this.state.currentlyPlaying;
    	const tooltip = isPlayed ? "Stop" : "Play";
    	const icon = isPlayed ? "pi pi-times" : "pi pi-caret-right"
    	const handler = isPlayed ? () => this.sendStopPlaybackRequest(changePlaybackState) :
    				() => this.sendStartPlaybackRequest(rowData, activeDeviceId, changePlaybackState);
        return (
        	<Button tooltip={tooltip} icon={icon} className="p-button-warning" style={{'margin' : '0 5px'}} onClick={handler} />
        );
    }

    titleTemplate(rowData, column)
    {
    	const type = rowData.type;
    	const details = rowData.details;
    	if (type === 'TRACK') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.artist}</i> from album <i>{details.album}, {details.year}</i>
				</>
			);
    	} else if (type === 'ALBUM') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.artist}, {details.year}</i>
				</>
			);
    	} else if (type === 'ARTIST') {
			return <b>{rowData.title}</b>;
		} else if (type === 'PLAYLIST') {
			return (
				<>
					<b>{rowData.title}</b> by <i>{details.owner}</i>
				</>
			);
		} else {
			return "";
		}
    }

    typeTemplate(rowData, color)
    {
    	return _.startCase(_.toLower(rowData.type));
    }

    publishPlaylist(playlistId, isPublic)
    {
    	const freshCopy = _.cloneDeep(this.state.items);
    	const index = _.findIndex(freshCopy.items, {'id' : playlistId});
      	freshCopy.items[index].public = isPublic;
      	this.setState({items: freshCopy});
    }

    prepareFollowButton(rowData)
    {
    	const trimmedSpotifyURI = this.trimSpotifyURIForPlaylist(rowData.spotifyURI);
    	if (rowData.type === 'PLAYLIST' && rowData.details.followed === true && rowData.details.ownerId !== CONFIG.userId) {
    		return (
    			<Button tooltip="Un-follow playlist" icon="pi pi-eye-slash" className="p-button-warning ml-5"
    				onClick={() => this.unfollowPlaylist(trimmedSpotifyURI)}
    			/>
			);
		} else if (rowData.type === 'PLAYLIST' && rowData.details.followed === false && rowData.details.ownerId !== CONFIG.userId) {
			return (
				<Button tooltip="Follow playlist" icon="pi pi-eye" className="p-button-warning ml-5"
					onClick={() => this.followPlaylist(trimmedSpotifyURI)}
				/>
			);
		} else {
			return "";
		}
    }

    trimSpotifyURIForPlaylist(uri) {
    	return uri.replace("spotify:playlist:", "");
    }

	actionsTemplate(rowData, column)
	{
		const followButton = this.prepareFollowButton(rowData);
		return (
			<>
				<Button tooltip="Sharing" icon="pi pi-share-alt" className="p-button-info" />
				{followButton}
				<CopyToClipboard onCopy={this.showCopyConfirmation} text={rowData.spotifyURI}>
                	<Button tooltip="Copy Spotify URI" icon="pi pi-copy" className="p-button-warning" style={{'margin' : '0 5px'}}/>
				</CopyToClipboard>
				<Button tooltip="Remove content" icon="pi pi-trash" className="p-button-danger" />
			</>
		);
	}

    prepareHeader()
    {
    	return (
    		<div className="p-clearfix" style={{'lineHeight':'1.87em'}}>
    			<span className="pi pi-star" style={{  'float' : 'left', 'fontSize': '2em'}}></span>
    			<span style={{ 'float' : 'left', 'fontSize': '1.5em'}}>Box</span>
    			<div style={{ 'float' : 'right'}}>
					<InputText type="search" onInput={(e) => this.setState({filter: e.target.value})} placeholder="Filter..." size="30"/>
    				<Button label="Refresh" icon="pi pi-refresh" style={{'marginLeft' : '5px'}} />
    			</div>
			</div>
    	);
    }

   render()
   {
   		const {activeDeviceId, changePlaybackState} = this.props;
        const header = this.prepareHeader();
        return (
			<div className="contentTable">
				<Growl ref={(el) => this.growl = el} />
				<div className="content-section implementation">
					<DataTable globalFilter={this.state.filter} value={this.state.items} header={header}
						emptyMessage="No items found" scrollable={true} scrollHeight="30em">
						<Column header="Playback" body={(r,c) => this.playbackTemplate(r,c,activeDeviceId, changePlaybackState)}
							style={{width: '6em'}} />
						<Column header="Title" field="title" body={this.titleTemplate} />
						<Column body={this.typeTemplate} field="type" header="Type" style={{width: '12em'}} />
						<Column field="shares.users" header="Shared with users" style={{width: '12em'}}/>
						<Column field="shares.groups" header="Shared with groups" style={{width: '12em'}} />
						<Column header="Actions" body={this.actionsTemplate} style={{width: '13em'}} />
					</DataTable>
				</div>
			</div>
        );
   }
}