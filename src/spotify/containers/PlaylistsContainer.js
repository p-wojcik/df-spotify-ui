import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
import {InputSwitch} from 'primereact/inputswitch';
import {InputText} from 'primereact/inputtext';
import {Growl} from 'primereact/growl';
import {CreatePlaylistDialog} from '../components/dialogs/CreatePlaylistDialog.js';
import {RemovePlaylistDialog} from '../components/dialogs/RemovePlaylistDialog.js';
import autoBind from 'react-autobind';

const { PlaylistsAPI } = require('../api/PlaylistsAPI.js');
const _ = require('lodash');
const CONFIG = require('../../config.json');

export class PlaylistsContainer extends Component {

    constructor(props) {
        super(props);
        this.defaultNewPlaylistDetails = {
			name: "",
			description: "",
			"public": false
		};
        this.state = {
        	filter: "",
        	playlists: {},
            removePlaylistDialogVisible : false,
            createPlaylistDialogVisible : false,
            removePlaylistDetails: {
				id: "",
				name: ""
			},
            newPlaylistDetails: _.clone(this.defaultNewPlaylistDetails)
        };
        this.api = new PlaylistsAPI();
        autoBind(this);
    }

    componentDidMount() {
      	this.fetchPlaylists(true);
    }

	fetchPlaylists(noGrowl) {
		this.api.fetchPlaylists((r) => {
			this.setState({playlists: r.data});
			if (!noGrowl) {
				this.showCustomGrowl("Playlists reloaded successfully!", 'Success', 'success');
			}
		}, this.showCustomGrowl);
	}

    createPlaylist() {
		this.api.createPlaylist(this.state.newPlaylistDetails, (r) => {
			this.fetchPlaylists(true).then(() => {
				this.setState({createPlaylistDialogVisible: false});
				const msg = "Playlist '" + this.state.newPlaylistDetails.name + "' created successfully!";
				this.showSuccessGrowl(msg);
			});
    	}, (e1, e2, e3) => {
    		this.showCustomGrowl(e1, e2, e3);
    		this.setState({createPlaylistDialogVisible: false})
    	});
    }

    removePlaylist() {
    	this.api.removePlaylist(this.state.removePlaylistDetails.id, (r) => {
    		this.fetchPlaylists(true).then(() => {
				this.setState({removePlaylistDialogVisible: false});
				const msg = "Playlist '" + this.state.removePlaylistDetails.name + "' removed successfully!";
				this.showSuccessGrowl(msg);
			});
		}, (e1, e2, e3) => {
			this.showCustomGrowl(e1, e2, e3);
			this.setState({removePlaylistDialogVisible: false})
		});
    }

    unfollowPlaylist(playlistId) {
    	this.api.unfollowPlaylist(playlistId, () => {
    		this.showCustomGrowl('Playlist has been un-followed.', 'Success', 'success');
    		this.fetchPlaylists(true);
		}, this.showCustomGrowl);
    }

    showCustomGrowl(msg, title, type) {
		this.growl.show({severity: type, summary: title, detail: msg});
    }

	showSuccessGrowl(msg)
	{
		this.showCustomGrowl(msg, 'Success', 'success');
	}

    publicTemplate(rowData, column) {
        const isOwned = rowData.owner.id === CONFIG.userId;
        const tooltip = isOwned ? "" : "Cannot un-publish someone's playlist.";
        return (
        	<InputSwitch tooltip={tooltip} disabled={!isOwned} checked={rowData.public}
        				 onChange={(e) => this.publishPlaylist(rowData.id, e.value)} />
        );
    }

    descriptionTemplate(rowData, column) {
    	return _.isEmpty(rowData.description) ? '-' : rowData.description;
    }

    publishPlaylist(playlistId, isPublic)
    {
    	const callback = () => {
    		const freshCopy = _.cloneDeep(this.state.playlists);
			const index = _.findIndex(freshCopy.items, {'id' : playlistId});
			freshCopy.items[index].public = isPublic;
			this.setState({playlists : freshCopy});
    	};

    	if (isPublic) {
    		this.api.makePlaylistPublic(playlistId, callback, this.showCustomGrowl);
    	} else {
    		this.api.makePlaylistPrivate(playlistId, callback, this.showCustomGrowl);
    	}
    }

	actionsTemplate(rowData, column) {
		const isOwned = rowData.owner.id === CONFIG.userId;
		const unfollowBtn = isOwned ? "" : (
			<Button tooltip="Unfollow" icon="pi pi-eye-slash" className="p-button-warning"
				onClick={() => this.unfollowPlaylist(rowData.id)} />
			);

		const playlistStateDetails = {
			removePlaylistDialogVisible: true,
			removePlaylistDetails: {
				id: rowData.id,
				name: rowData.name
			}
		};
		const deleteBtn = !isOwned ? "" : <Button tooltip="Remove playlist" icon="pi pi-trash" className="p-button-danger"
			onClick={() => this.setState(playlistStateDetails)} />;

		return (
			<>
				<Button tooltip="Add to my Box" icon="pi pi-star" className="p-button-info"  style={{'marginRight' : '5px'}}/>
				{unfollowBtn}
				{deleteBtn}
			</>
		);
	}

    prepareHeader()
    {
    	const freshState = {
    		createPlaylistDialogVisible: true,
    		newPlaylistDetails: _.clone(this.defaultNewPlaylistDetails)
		};

    	return (
			<div className="p-clearfix" style={{'lineHeight':'1.87em'}}>
				<span className="pi pi-list" style={{ 'float' : 'left', 'fontSize': '2em'}}></span>
				<span style={{ 'float' : 'left', 'fontSize': '1.5em'}}>Playlists</span>
				<div style={{ 'float' : 'right'}}>
					<InputText type="search" onInput={(e) => this.setState({filter: e.target.value})} placeholder="Filter..." size="30"/>
					<Button label="Create" icon="pi pi-plus"
							style={{'margin' : '0 5px'}}
							onClick={() => this.setState(freshState)}
					/>
					<Button label="Refresh" icon="pi pi-refresh" onClick={(e) => {
						e.persist();
						this.fetchPlaylists();
					}} />
				</div>
			</div>
    	);
    }

    render() {
        const header = this.prepareHeader();
        return (
			<>
				<Growl ref={(el) => this.growl = el} />
				<CreatePlaylistDialog visible={this.state.createPlaylistDialogVisible}
									  changeVisibility={(e) => this.setState({createPlaylistDialogVisible : e})}
									  detailsHolder={this.state.newPlaylistDetails}
									  onChange={(e) => this.setState({newPlaylistDetails: e})}
									  onSave={this.createPlaylist}
			  	/>
			  	<RemovePlaylistDialog visible={this.state.removePlaylistDialogVisible}
									  changeVisibility={(e) => this.setState({removePlaylistDialogVisible : e})}
			  						  detailsHolder={this.state.removePlaylistDetails}
			  						  onAccept={this.removePlaylist}
			  	/>

				<div className="contentTable">
					<div className="content-section implementation">
						<DataTable globalFilter={this.state.filter} value={this.state.playlists.items} header={header}
							emptyMessage="No playlists found" scrollable={true} scrollHeight="20em">
							<Column header="Public" body={this.publicTemplate} style={{width: '8em'}} />
							<Column field="name" header="Name"  />
							<Column field="description" body={this.descriptionTemplate} header="Description" />
							<Column field="owner.displayName" header="Owner" />
							<Column header="Actions" body={this.actionsTemplate} style={{width: '7em'}} />
						</DataTable>
					</div>
				</div>
			</>
        );
    }
}