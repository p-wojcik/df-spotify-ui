import React, { Component } from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
import {InputSwitch} from 'primereact/inputswitch';
import {InputText} from 'primereact/inputtext';
import {Slider} from 'primereact/slider';
import {Growl} from 'primereact/growl';
import autoBind from 'react-autobind';
const {DevicesAPI } = require('../api/DevicesAPI.js');
const _ = require('lodash');
const CONFIG = require('../../config.json');

export class DevicesContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
        	filter: "",
            devices: []
        };
		this.api = new DevicesAPI();
        autoBind(this);
    }

	componentDidMount() {
		this.fetchDevices(true);
	}

	fetchDevices(noGrowl) {
		this.api.fetchDevices((r) => {
			this.setState({devices: r.data});
			if (!noGrowl) {
				const msg = "Devices reloaded successfully!";
				this.showCustomGrowl(msg, 'Success', 'success');
			}
		}, this.showCustomGrowl);
	}

	showCustomGrowl(msg, title, type) {
		this.growl.show({severity: type, summary: title, detail: msg});
	}

    volumeTemplate(rowData, column) {
        return (
			<Slider value={rowData.volumePercent} step={CONFIG.volumeChangeStepPercent}
					onChange={(e) => this.setVolume(rowData.id, e.value)} />
        );
    }

    setVolume(deviceId, volume) {
    	const freshCopy = _.slice(this.state.devices);
    	const index = _.findIndex(freshCopy, {'id' : deviceId});
    	if (freshCopy[index].volumePercent !== volume) {
      		freshCopy[index].volumePercent = volume;
      		this.sendRestVolumeUpdate(deviceId, volume, () => this.setState({devices: freshCopy}));
    	}
    }

    sendRestVolumeUpdate(deviceId, volumePercent, callback) {
    	this.api.volumeUpdateRequest(deviceId, volumePercent, callback, this.showCustomGrowl);
    }

	transferPlayback(deviceId) {
		this.api.transferPlayback(deviceId, this.showCustomGrowl);
	}

    statusTemplate(rowData, column, setActiveDeviceCallback, currentlyPlaying) {
    	const tooltip = !!rowData.active ? "Device will be inactivated automatically after activating other device." : "";
        return (
        	<InputSwitch tooltip={tooltip} disabled={rowData.active} checked={rowData.active}
        		onChange={() => {
					this.transferPlayback(rowData.id);
        			this.setActiveDevice(rowData.id, setActiveDeviceCallback, currentlyPlaying);
				}} />
        );
    }

    setActiveDevice(deviceId, setActiveDeviceCallback, currentlyPlaying)
    {
    	setActiveDeviceCallback(deviceId);
    	const freshCopy = _.slice(this.state.devices);
    	_.forEach(freshCopy, d => {
    		d.active = d.id === deviceId;
    	});
		this.setState({devices: freshCopy });
    }

    prepareHeader()
    {
    	return (
    		<div className="p-clearfix" style={{'lineHeight':'1.87em'}}>
				<span className="pi pi-mobile" style={{ 'float' : 'left', 'fontSize': '2em'}}></span>
				<span style={{ 'float' : 'left', 'fontSize': '1.5em'}}>Devices</span>
				<div style={{ 'float' : 'right'}}>
					<InputText type="search" onInput={(e) => this.setState({filter: e.target.value})} placeholder="Filter..." size="30"/>
    				<Button label="Refresh" icon="pi pi-refresh" style={{'marginLeft':'5px'}} onClick={() => this.fetchDevices()}/>
    			</div>
			</div>
    	);
    }

    render() {
    	const {setActiveDeviceCallback, currentlyPlaying} = this.props;
        const header = this.prepareHeader();
        return (
        	<>
				<Growl ref={(el) => this.growl = el} />
				<div className="contentTable">
					<div className="content-section implementation">
						<DataTable globalFilter={this.state.filter} value={this.state.devices} header={header}
							emptyMessage="No connected devices found" scrollable={true} scrollHeight="20em">
							<Column header="Active" body={(r,c) => this.statusTemplate(r,c, setActiveDeviceCallback, currentlyPlaying)}
								style={{width: '8em'}}/>
							<Column field="name" header="Name" />
							<Column field="type" header="Type" />
							<Column header="Volume" body={this.volumeTemplate} />
						</DataTable>
					</div>
				</div>
            </>
        );
    }
}