import React from 'react';
import {Card} from 'primereact/card';
import {Button} from 'primereact/button';

export function BrowseSpotifyHeaderComponent() {
    const footer = (
    	<a href='https://open.spotify.com' target="_blank" rel="noopener noreferrer">
    		<Button label="Browse" icon="pi pi-external-link" style={{marginRight: '.25em'}} />
    	</a>);
	return (
		<Card footer={footer} title="Browse in Spotify" style={{textAlign: 'left'}}>
			Browse Spotify content that you want to be shared here! For given album, playlist, artist or track, just copy the
			related <b>SpotifyURI</b> or link to the material and add it here using panel on the right.
		</Card>
	);
}