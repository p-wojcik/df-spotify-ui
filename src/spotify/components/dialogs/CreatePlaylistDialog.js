import React from 'react';
import {Dialog} from 'primereact/dialog';
import {InputText} from 'primereact/inputtext';
import {Checkbox} from 'primereact/checkbox';
import {Button} from 'primereact/button';

export function CreatePlaylistDialog(props) {
	const { detailsHolder, onChange, onSave, visible, changeVisibility } = props;

	const footer = (<Button label="Save" icon="pi pi-check" onClick={onSave} /> );

	return (
		<Dialog header="Create playlist" footer={footer} visible={visible} modal={true} style={{width: '50vw'}}
				onHide={() => changeVisibility(false)}>
			Type the details for the new playlist:
			<span className="p-float-label" style={{ marginTop: '20px'}}>
				<InputText id="newPlaylistName" value={detailsHolder.name}
					onChange={(e) => { onChange(Object.assign({}, detailsHolder, {name: e.target.value}));}}
				/>
				<label htmlFor="newPlaylistName">Name</label>
			</span>
			<span className="p-float-label" style={{ marginTop: '20px'}}>
				<InputText id="newPlaylistDesc" value={detailsHolder.description}
					onChange={(e) => { onChange(Object.assign({}, detailsHolder, {description: e.target.value}));}}
				/>
				<label htmlFor="newPlaylistDesc">Description</label>
			</span>
			<div style={{ marginTop: '20px'}}>
				<Checkbox id="newPlaylistPublicState" checked={detailsHolder['public']}
					onChange={e => { onChange(Object.assign({}, detailsHolder, {"public": e.checked})); }}
				></Checkbox>
				<label htmlFor="newPlaylistPublicState" className="p-checkbox-label">Public</label>
			</div>
		</Dialog>
	);
}