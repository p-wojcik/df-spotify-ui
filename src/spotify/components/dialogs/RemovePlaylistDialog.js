import React from 'react';
import {Dialog} from 'primereact/dialog';
import {Button} from 'primereact/button';

export function RemovePlaylistDialog(props) {

	const { detailsHolder, onAccept, visible, changeVisibility } = props;

	const footer = (
		<div>
			<Button label="Yes" icon="pi pi-check" onClick={onAccept} />
			<Button label="No" icon="pi pi-times" className="p-button-danger" onClick={() => changeVisibility(false)} />
		</div>
	);

	return (
		<Dialog header="Remove playlist" footer={footer} visible={visible} style={{width: '50vw'}} modal={true}
				onHide={() => changeVisibility(false)}>
			Do you really want to remove playlist <i>{detailsHolder.name}</i> ?
		</Dialog>
	);
}