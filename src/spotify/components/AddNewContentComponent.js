import React from 'react';
import {Card} from 'primereact/card';
import {Button} from 'primereact/button';
import {InputText} from 'primereact/inputtext';

export function AddNewContentComponent(props) {
    const footer = (
    	<>
			<Button label="Add content" icon="pi pi-star" style={{marginRight: '.25em'}} />
			<div className="p-inputgroup" style={{ display: "contents"}}>
				<span className="p-inputgroup-addon" style={{marginLeft: ".25em"}}>
					<i className="pi pi-chevron-right"></i>
				</span>
				<InputText placeholder="Paste URI here..." style={{ width: "75%"}}/>
			</div>
    	</>
    );
	return (
		<Card footer={footer} title="Add new content" style={{textAlign: 'left'}}>
			Add your favourite Spotify content here to share it with your friends!
			Just paste related SpotifyURI or link to the content and click "Add new content" button!
		</Card>
	);
}