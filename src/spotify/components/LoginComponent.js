import React, { Component } from 'react';
import {Card} from 'primereact/card';
import {Button} from 'primereact/button';
import autoBind from 'react-autobind';
const CONFIG = require('../../config.json');

export class LoginComponent extends Component {

	constructor(props) {
		super(props);
		this.authorizationUri = CONFIG.spotifyApp.baseUrl + '/' + CONFIG.spotifyApp.endpoints.authorization;
		autoBind(this);
	}

	render() {
		const footer = (
			<a href={this.authorizationUri}>
				<Button label="Login" icon="pi pi-external-link" style={{margin: '2em 0 1em 0'}} />
			</a>
		);

		return (
			<Card footer={footer} title="Login to Spotify" style={{textAlign: 'center', width: "20%", margin: "2em auto"}}>
				<p>Browse Spotify and share your favourite content with friends!
				In order to start using Spotify integration, log into your Spotify account using button below.
				</p><p>To use this feature, Spotify Premium account is required.</p>
			</Card>
		);
	}
}