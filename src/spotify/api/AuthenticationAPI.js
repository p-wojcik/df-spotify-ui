import {ErrorHandler} from '../../util/ErrorHandler.js';

const _ = require('lodash');
const axios = require('axios');
const CONFIG = require('../../config.json');

export class AuthenticationAPI {

 	constructor() {
        this.restClient = axios.create({ baseURL: CONFIG.spotifyApp.baseUrl });
        this.introducePath = _.replace(CONFIG.spotifyApp.endpoints.introduce, '<userId>', CONFIG.userId);
    }

	introduce(callback, errorCallback) {
		this.restClient.head(this.introducePath)
				.then(callback)
				.catch((e) => ErrorHandler.handleError(e, errorCallback));
    }
}