import {ErrorHandler} from '../../util/ErrorHandler.js';

const _ = require('lodash');
const axios = require('axios');
const CONFIG = require('../../config.json');

export class PlaybackAPI {

 	constructor() {
        this.restClient = axios.create({ baseURL: CONFIG.spotifyApp.baseUrl });
        this.playbackPath = _.replace(CONFIG.spotifyApp.endpoints.playback, '<userId>', CONFIG.userId);
    }

	startPlayback(item, activeDeviceId, callback, errorCallback) {
    	const target = (item.type === 'TRACK') ? 'TRACK' : 'CONTEXT';
    	const payload = { id: item.spotifyURI, type: "DIRECT", target: target};
    	const queryParams = { device: activeDeviceId};
		this.restClient.put(this.playbackPath, payload, {params: queryParams})
				.then(callback)
				.catch((e) => ErrorHandler.handleError(e, errorCallback));
    }

	stopPlayback(callback, errorCallback) {
		this.restClient.put(this.playbackPath + "/pause")
				.then(callback)
				.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}
}