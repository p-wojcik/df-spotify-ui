import {ErrorHandler} from '../../util/ErrorHandler.js';

const _ = require('lodash');
const axios = require('axios');
const CONFIG = require('../../config.json');

export class DevicesAPI {

 	constructor() {
        this.restClient = axios.create({ baseURL: CONFIG.spotifyApp.baseUrl });
        this.devicesPath = _.replace(CONFIG.spotifyApp.endpoints.devices, '<userId>', CONFIG.userId);
        this.transferPath = _.replace(CONFIG.spotifyApp.endpoints.playback, '<userId>', CONFIG.userId) + '/transfer';
    }

    fetchDevices(callback, errorCallback) {
		this.restClient.get(this.devicesPath)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
    }

	volumeUpdateRequest(deviceId, volumePercent, callback, errorCallback) {
		const url = this.devicesPath + "/" + deviceId;
		this.restClient.put(url, { volumePercent })
				.then(callback())
				.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

	transferPlayback(deviceId, errorCallback) {
		const queryParams = { device: deviceId};
		this.restClient.put(this.transferPath, {}, { params: queryParams})
				.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}
}