import {ErrorHandler} from '../../util/ErrorHandler.js';

const _ = require('lodash');
const axios = require('axios');
const CONFIG = require('../../config.json');

export class PlaylistsAPI {

 	constructor() {
        this.restClient = axios.create({ baseURL: CONFIG.spotifyApp.baseUrl });
        this.playlistsPath = _.replace(CONFIG.spotifyApp.endpoints.playlists, '<userId>', CONFIG.userId);
    }

    fetchPlaylists(callback, errorCallback) {
    	this.restClient.get(this.playlistsPath)
    		.then(callback)
    		.catch((e) => ErrorHandler.handleError(e, errorCallback));
    }

    removePlaylist(playlistId, callback, errorCallback) {
    	const url = this.playlistsPath + '/' + playlistId;
		this.restClient.delete(url)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

	createPlaylist(details, callback, errorCallback) {
		this.restClient.post(this.playlistsPath, details)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

	makePlaylistPublic(playlistId, callback, errorCallback) {
		const url = this.playlistsPath + '/' + playlistId + '/publish';
		this.restClient.put(url)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

	makePlaylistPrivate(playlistId, callback, errorCallback) {
		const url = this.playlistsPath + '/' + playlistId + '/publish';
		this.restClient.delete(url)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

    followPlaylist(playlistId, callback, errorCallback) {
    	const url = this.playlistsPath + '/' + playlistId + '/follow';
		this.restClient.put(url)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}

    unfollowPlaylist(playlistId, callback, errorCallback) {
    	const url = this.playlistsPath + '/' + playlistId + '/follow';
		this.restClient.delete(url)
			.then(callback)
			.catch((e) => ErrorHandler.handleError(e, errorCallback));
	}
}