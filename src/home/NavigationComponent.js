import React from 'react';
import {Sidebar} from 'primereact/sidebar';
import {Menu} from 'primereact/menu';

export default function NavigationComponent(props)
{
	const items = [
		 {
			 label: 'Home',
			 items: [
				{label: 'Dashboard', icon: 'pi pi-fw pi-home', url: '/'}
			 ]
		 },
		 {
			 label: 'Content',
			 icon: 'pi pi-fw pi-inbox',
			 items: [
			 	{label: 'Notes', icon: 'pi pi-fw pi-pencil', command: () => { props.onHide(); }},
				{label: 'Reminder', icon: 'pi pi-fw pi-calendar', command: () => { props.onHide(); }},
				{label: 'Files', icon: 'pi pi-fw pi-file', command: () => { props.onHide(); }},
				{label: 'E-books', icon: 'pi pi-fw pi-file-pdf', command: () => { props.onHide(); }},
				{label: 'Spotify', icon: 'pi pi-fw pi-volume-down', url: '/secure/spotify'},
			 ]
		 },
		 {
			 label: 'Settings',
			 items: [
				{label: 'Profile', icon: 'pi pi-fw pi-user', command: () => { props.onHide(); }},
			 	{label: 'Groups', icon: 'pi pi-fw pi-users', command: () => { props.onHide(); }},
			 	{label: 'Notifications', icon: 'pi pi-fw pi-sliders-v', command: () => { props.onHide(); }},
			 ]
		 },
		 {
			 label: 'Sign out',
			 items: [
				{label: 'Logout', icon: 'pi pi-fw pi-power-off', command: () => { props.onLogout(); props.onHide(); }}
			 ]
		 }
	];

	return (
		<Sidebar visible={props.visible} onHide={props.onHide}>
			 <h1 id="sidebar-title">Menu</h1>
			 <Menu model={items} />
		</Sidebar>
	);
}