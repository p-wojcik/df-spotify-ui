import React, { Component } from 'react';
import { withAuth } from '@okta/okta-react';
import './../App.css';
import NavigationComponent from './NavigationComponent';
import Login from './Login';
import HeaderComponent from './HeaderComponent';
import {RootContainer as SpotifyRootContainer} from '../spotify/containers/RootContainer.js'
import {Button} from 'primereact/button';
import { Route } from 'react-router-dom';
import { SecureRoute } from '@okta/okta-react';
import autoBind from 'react-autobind';

export default withAuth(class HomeComponent extends Component {
	constructor(props) {
		super(props);
		this.state = {
			authenticated: null,
			sidebarVisible: false
		};
		autoBind(this);
		this.checkAuthentication();
	}

  	async checkAuthentication() {
		const authenticated = await this.props.auth.isAuthenticated();
		if (authenticated !== this.state.authenticated) {
			this.setState({ authenticated });
		}
  	}

	componentDidUpdate() {
    	this.checkAuthentication();
  	}

  	onSidebarHide() {
  		this.setState({sidebarVisible: false});
  	}

  	onSidebarShow() {
  		this.setState({sidebarVisible:true});
  	}

  	onLogin() {
  		this.props.auth.login();
  	}

  	onLogout() {
  		this.props.auth.logout();
  	}

  	render() {
		if (this.state.authenticated === null) {
			return null;
		}

		const shouldShowSidebar = this.state.sidebarVisible && this.state.authenticated;
		return (
			<>
				<NavigationComponent visible={shouldShowSidebar} onHide={this.onSidebarHide} onLogout={this.onLogout} />

				<div id="rootContainer">
					<HeaderComponent
						authenticated={this.state.authenticated}
						onLogin={this.onLogin}
						onLogout={this.onLogout}
						onSidebarShow={this.onSidebarShow}
					/>
					<div id="rootContent">
						<Route path='/' exact={true} render={() => <Button label="xx" />} />
						<SecureRoute path='/secure/spotify' component={SpotifyRootContainer}/>
						<Route path='/login' render={() => <Login baseUrl='https://dev-607510.okta.com' />} />
					</div>
				</div>
			</>
		);
  	}
});