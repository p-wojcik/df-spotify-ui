import React from 'react';
import {Button} from 'primereact/button';

export default function HeaderComponent(props)
{
	const loginButton = props.authenticated ?
		<Button onClick={props.onLogout} label="Logout" icon="pi pi-power-off" className="login-button" /> :
		<Button onClick={props.onLogin} label="Login" className="login-button" />;

	const menuCssRule = props.authenticated ? 'visible' : "hidden";

	return (
		<div id="headerContainer">
			<div id="headerLeftContent">
				<Button id="headerLeftContentMenu" style={{visibility: menuCssRule}} icon="pi pi-bars" onClick={props.onSidebarShow} />
			</div>
			<div id="headerCenterContent">
				<span className="headingText">dataFlex</span>
			</div>
			<div id="headerRightContent">
				{loginButton}
			</div>
		</div>
	);
}